#!/usr/bin/env bash

source "$HOME/.aliases"
logfile="$HOME/.autorun.log"
title="autorun"

if lsof "$logfile"; then
    notify-send --urgency=normal "$title" "$logfile is open"
else
    rm "$logfile"
fi

function show-ok {
    notify-send --urgency=normal "$title" "started $1" 
}

function show-info {
    notify-send --urgency=low "$title" "$1 already running"
}

function default_test {
    ps aux | grep -wiF "$1" | grep -v grep|grep -vq ' man '
}

function run {
    NAME=$1
    COMMAND=${2:-"$NAME"}
    TEST=${3:-"default_test $NAME"}
    if eval "$TEST";
    then
    show-info "$NAME"
    # echo "$NAME already running" >> "$logfile"
    else
    echo "====$NAME====" >> "$logfile"
    eval "$COMMAND" 2>&1 | awk '{ print "\033[36m[" NAME "]\033[0m " $0 }' "NAME=$NAME" >> "$logfile"&
    show-ok "$NAME"
    fi
}

function test-network {
    local title="autorun - test network"
    echo "====ping====" >> "$logfile"
    ping -c 1 1.1.1.1 >> "$logfile"
    EXIT="$?"
    if [ "$EXIT" = "0" ]; then
    notify-send --urgency=normal "$title" "connected"
    return 0
    elif [ "$EXIT" = "1" ]; then
    notify-send --urgency=critical "$title" "Router has no internet access"
    return 1
    elif [ "$EXIT" = "2" ]; then
    notify-send --urgency=critical "$title" "Not connected to a network"
    return 1
    fi
}

function set-wacom-screen {
    errors=0
    xsetwacom set "Wacom Intuos S Pen stylus" MapToOutput HEAD-0 || { notify-send --urgency=critical "$title" "couldn't map wacom stylus"; errors=1; }
    # xsetwacom set "Wacom Intuos S Pen eraser" MapToOutput HEAD-0 || { notify-send --urgency=critical "$title" "couldn't map wacom eraser"; errors=1; }
    # xsetwacom set "Wacom Intuos S Pen cursor" MapToOutput HEAD-0 || { notify-send --urgency=critical "$title" "couldn't map wacom cursor"; errors=1; }
    if [ $errors -eq 1 ]; then
    notify-send --urgency=critical "$title" "couldn't fully map wacom tablet"
    return 1
    else
    notify-send --urgency=normal "$title" "mapped wacom tablet"
    fi
}

function create-sink {
    if pactl list sinks|grep -i "name: game_sink";
    then
    notify-send --urgency=low "$title" "game sink already exists"
    else
    pactl load-module module-null-sink sink_name=game_sink sink_properties=device.description=Game-Sink
    notify-send --urgency=normal "$title" "created sink"
    fi
}

function start-svm {
    if ssh root@svm1013.net.in.tum.de "echo hello"; then
    notify-send --urgency=low "$title" "svm already running"
    elif ssh svm@grnvs.net; then
    notify-send --urgency=normal "$title" "svm started"
    else
    notify-send --urgency=critical "$title" "failed to start svm"
    fi
}

function mount-svm {
    if [ -d ~/code/cpp/grnvs/svm/code ]; then
    notify-send --urgency=low "$title" "svm already mounted"
    elif sshfs jemand2001@svm1013.net.in.tum.de:/home/jemand2001 ~/code/cpp/grnvs/svm >> "$logfile"; then
    notify-send --urgency=normal "$title" "svm mounted"
    else
    notify-send --urgency=critical "$title" "failed to mount svm"
    return 1
    fi
}

test-network
netstatus="$?"

# set HDMI resolution (necessary when i was using a 4k tv sitting a meter away)
#if xrandr --output HDMI-0 --mode 1920x1080; then notify-send --urgency=normal "$title" "set display resolution to 1920x1080"; else notify-send --urgency=critical "$title" "couldn't set resolution"; fi
#xrandr --output HDMI-0 --primary --right-of DVI-I-1

# map tablet to the correct screen (necessary for 2 monitor setups)
#run-compton
run nm-applet
if which discord-canary; then
    DISCORD="discord-canary"
else
    DISCORD="discord"
fi

# [ $netstatus -eq 0 ] && run chrome google-chrome-stable 'ps aux | grep -wiF chrome | grep -v grep|grep -v " man "|grep -vq "discord"'
[ $netstatus -eq 0 ] && run firefox
[ $netstatus -eq 0 ] && run discord "$DISCORD" 'ps aux | grep -wiF discord | grep -v grep'
[ $netstatus -eq 0 ] && run nextcloud

# *i think* this should fix steam not showing a file picker?
# run /usr/lib/xdg-desktop-portal
# run /usr/lib/xdg-desktop-portal-gtk

run steam
run pasystray
# run-wicd
# [ $netstatus -eq 0 ] && run rocketchat
run flameshot
#run redshift
run blueman-applet
#run-talon
run keepassxc "" "ps x | grep -q 'keepassxc$'"
# add-keys
run openRGB "openrgb --startminimized -p ~/.config/OpenRGB/just\ red.orp"

run stalonetray
# run dunst

# start-svm && mount-svm
mkdir -p /tmp/tests
run ibus-daemon "ibus-daemon -rxR" false

sleep 1
create-sink
set-wacom-screen

if nmcli connection up 'wenn Bäume WLAN hätten';
then notify-send --urgency=normal "WiFi" "started WiFi network" 
else notify-send --urgency=critical "WiFi" "could not start WiFi network"
fi

disown -a
