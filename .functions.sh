#!/usr/bin/env sh

gitbranch(){
    ret=$(git branch 2> /dev/null | sed -n -e 's/* \(.*\)/\1/p')
    if [ -n "$ret" ]
    then
	case $- in
	    *i*) echo " [$ret]";;
	    *) echo $ret;;
	esac
    fi
}

# export LESS_TERMCAP_md=$'\e[1m\e[38;5;160m'
# export LESS_TERMCAP_us=$'\e[;4m\e[38;5;210m'
# export LESS_TERMCAP_so=$'\e[48;5;244m\e[30m'
# export LESS_TERMCAP_me=$'\e[0m'
# export LESS_TERMCAP_ue=$'\e[0m'
# export LESS_TERMCAP_se=$'\e[0m'

#fuzction make() {
#    for i in Makefile makefile GNUmakefile; do
#       if [ -f "$i" ]; then
#	    command make -f $i $@
#	    return $?
#	fi
#    done
#    command make -f ~/Makefile $@
#    return $?
#}

alias make="make -f ~/Makefile -j16"

function headphones {
    case $1 in
	connect)
	    echo "connect 99:72:31:00:43:53" | bluetoothctl
	    return $?;;
	disconnect)
	    echo "disconnect 99:72:31:00:43:53" | bluetoothctl
	    return $?;;
    esac
    echo "Usage: $0 <connect|disconnect>"
    if [ -n "$1" ]; then return 1; else return 0; fi
}

function reload {
	source "$HOME/.$(basename $(readlink /proc/$$/exe))rc"
}

